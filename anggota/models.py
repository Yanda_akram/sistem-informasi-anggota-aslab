from django.db import models
from django.contrib.auth.models import User
from django.utils import timezone
from django.urls import reverse #new

#Create your models here.
class Anggota(models.Model):
    JURUSAN_CHOICES = (
        ('ak','Akuntansi'),
        ('mi','Manajemen Informatika'),
        ('me','Mesin'),
    )
    nama_anggota = models.CharField('Nama Anggota', max_length=50, null=False)
    alamat = models.TextField()
    jurusan = models.CharField(max_length=2, choices=JURUSAN_CHOICES)
    nohp = models.CharField('No HP',max_length=15)   
    tgl_input = models.DateTimeField('Tgl. Edit',default=timezone.now)
    user = models.ForeignKey(User, on_delete=models.DO_NOTHING)

    class Meta:
        ordering = ['-tgl_input']

    def __str__(self):
        return self.nama_anggota

    def get_absolute_url(self): #new
        return reverse('home_page')
