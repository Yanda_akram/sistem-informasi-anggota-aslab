from django.urls import path
from .views import index, AnggotaDetailView, AnggotaCreateView #detail anggota, create
from .views import AnggotaEditView, AnggotaDeleteView, AnggotaToPdf #new Edit


urlpatterns = [
    path('', index, name='home_page'),
    path('anggota/<int:pk>', AnggotaDetailView.as_view(), #new detail
        name='anggota_detail_view'),
    path('anggota/add', AnggotaCreateView.as_view(), name='anggota_add'),
    path('anggota/edit/<int:pk>', AnggotaEditView.as_view(), name='anggota_edit'),
    #new edit anggota
    path('anggota/delete/<int:pk>', AnggotaDeleteView.as_view(), name='anggota_delete'),
    #new edit anggota
    path('anggota/print_pdf', AnggotaToPdf.as_view(), name='anggota_to_pdf')
    #new edit anggota
]
