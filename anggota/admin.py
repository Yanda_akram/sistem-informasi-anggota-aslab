from django.contrib import admin
from .models import Anggota

# Register your models here.
@admin.register(Anggota)
class AnggotaAdmin(admin.ModelAdmin):
    list_display = ['nama_anggota','alamat','jurusan','nohp',
                    'tgl_input','user']
    list_filter = ['nama_anggota','jurusan','nohp','user']
    search_fields = ['nama_anggota','jurusan','user']
