from django.shortcuts import render
from .models import Anggota #new anggota
from django.views.generic import DetailView,CreateView #new detail, create
from django.views.generic import UpdateView
from django.urls import reverse_lazy #new delete
from django.views.generic import DeleteView
from .utils import Render #export pdf
from django.views.generic import View

# Create your views here.

var = {
    'judul' : 'SISTEM INFORMASI KEANGGOTAAN ASLAB GI-BEI',
     'info' : '''Kami menyediakan informasi mengenai keanggotaan GI-BEI yang ada pada POLINEMA PSDKU KEDIRI''',
     'oleh' : 'yanda-satrio'
}
def index(self):
    #new index anggota
    var['anggota'] = Anggota.objects.values('id','nama_anggota','jurusan').\
         order_by('nama_anggota')
    return render(self, 'anggota/index.html',context=var)

class AnggotaDetailView(DetailView): #new detail anggota
    model = Anggota
    template_name = 'anggota/anggota_detail_view.html'

    def get_context_data(self, **kwargs):
        context = var
        context.update(super().get_context_data(**kwargs))
        return context
class AnggotaCreateView(CreateView): #new add anggota
    model = Anggota
    fields = '__all__'
    template_name = 'anggota/anggota_add.html'

    def get_context_data(self, **kwargs):
        context = var
        context.update(super().get_context_data(**kwargs))
        return context

class AnggotaEditView(UpdateView):
    model = Anggota
    fields = ['nama_anggota','jurusan','alamat',
              'nohp']
    template_name = 'anggota/anggota_edit.html'

    def get_context_data(self, **kwargs):
        context = var
        context.update(super().get_context_data(**kwargs))
        return context
    
class AnggotaDeleteView(DeleteView):
    model = Anggota
    template_name = 'anggota/anggota_delete.html'
    success_url = reverse_lazy('home_page')

    def get_context_data(self, **kwargs):
        context = var
        context.update(super().get_context_data(**kwargs))
        return context

class AnggotaToPdf(View):
    def get(self,request):
        var = {
            'anggota' : Anggota.objects.values(
                'nama_anggota','alamat','jurusan','nohp'),
            'request':request
        }
        return Render.to_pdf(self,'anggota/anggota_to_pdf.html',var)





